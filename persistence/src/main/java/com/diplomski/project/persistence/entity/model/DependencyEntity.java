package com.diplomski.project.persistence.entity.model;

import com.diplomski.project.business.model.Dependency;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "dependency")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DependencyEntity extends BaseTimestampedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dependency_id_gen")
    @SequenceGenerator(name = "dependency_id_gen", sequenceName = "dependency_id_seq", allocationSize = 1)
    private Long id;

    private String name;

    @Column(name = "dependency_value")
    private String value;

    public DependencyEntity(final Dependency dependency) {
        this.id = null;
        this.name = dependency.getName();
        this.value = dependency.getValue();
    }

    public Dependency toDependency() {
        return new Dependency(
                this.id.toString(),
                this.name,
                this.value);
    }
}
