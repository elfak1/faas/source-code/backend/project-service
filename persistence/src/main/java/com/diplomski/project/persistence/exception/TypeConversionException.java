package com.diplomski.project.persistence.exception;

public class TypeConversionException extends RuntimeException {

    public TypeConversionException(NumberFormatException nfe) {
        super(nfe.getMessage());
    }
}
