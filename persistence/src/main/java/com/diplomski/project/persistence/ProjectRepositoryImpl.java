package com.diplomski.project.persistence;

import com.diplomski.project.business.boundary.repository.ProjectRepository;
import com.diplomski.project.business.boundary.repository.request.FindProjectRepoRequest;
import com.diplomski.project.business.boundary.repository.response.FindProjectRepoResponse;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.persistence.entity.model.ProjectEntity;
import com.diplomski.project.persistence.entity.repository.ProjectEntityRepository;
import com.diplomski.project.persistence.util.TypeConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class ProjectRepositoryImpl implements ProjectRepository {

    private final ProjectEntityRepository projectEntityRepository;

    @Override
    @Transactional
    public Project saveProject(Project project) {
        final ProjectEntity projectEntity = new ProjectEntity(project);

        projectEntityRepository.save(projectEntity);

        return projectEntity.toProject();
    }

    @Override
    @Transactional
    public void deleteProjectById(String projectId) {
        final Long longProjectId = TypeConverter.string2Long(projectId);

        projectEntityRepository.deleteById(longProjectId);
    }

    @Override
    @Transactional
    public Project setProjectDeployment(String projectId, Deployment deployment) {
        final Long longProjectId = TypeConverter.string2Long(projectId);

        ProjectEntity projectEntity = projectEntityRepository.getById(longProjectId);

        projectEntity.setDeploymentId(projectEntityRepository.getNextDeploymentId());
        projectEntity.setExternalId(deployment.getExternalId());
        projectEntity.setRepositoryUrl(deployment.getRepositoryUrl());
        projectEntity.setDeploymentStatus(deployment.getDeploymentStatus());
        projectEntity.setPipelineTriggerToken(deployment.getPipelineTriggerToken());
        projectEntity.setScaleFactor(deployment.getScaleFactor());

        projectEntityRepository.save(projectEntity);

        return projectEntity.toProject();
    }

    @Override
    public boolean existsByProjectNameAndProfileId(String projectName, String profileId) {
        final Long longProfileId = TypeConverter.string2Long(profileId);

        return projectEntityRepository.existsByProfileIdAndName(longProfileId, projectName);
    }

    @Override
    public FindProjectRepoResponse findProject(FindProjectRepoRequest findProjectRepoRequest) {
        final Long longProjectId = TypeConverter.string2Long(findProjectRepoRequest.getProjectId());

        Optional<ProjectEntity> projectEntityOptional = projectEntityRepository.findById(longProjectId);

        Optional<Project> projectOptional = projectEntityOptional
                .map(ProjectEntity::toProject);

        return FindProjectRepoResponse.builder()
                .projectOptional(projectOptional)
                .build();
    }

    @Override
    public Page<Project> findByProjectOwner(final String projectOwnerId, final Pageable pageable) {
        final Long longProjectId = TypeConverter.string2Long(projectOwnerId);

        Page<ProjectEntity> projectEntityPage = projectEntityRepository.findByProjectOwner(longProjectId, pageable);

        return projectEntityPage.map(ProjectEntity::toProject);
    }
}
