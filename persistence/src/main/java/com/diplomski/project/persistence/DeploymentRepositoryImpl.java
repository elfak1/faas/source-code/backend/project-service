package com.diplomski.project.persistence;

import com.diplomski.project.business.boundary.repository.DeploymentRepository;
import com.diplomski.project.business.exception.ItemNotFoundException;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.model.value.DeploymentStatus;
import com.diplomski.project.persistence.entity.model.ProjectEntity;
import com.diplomski.project.persistence.entity.repository.ProjectEntityRepository;
import com.diplomski.project.persistence.util.TypeConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class DeploymentRepositoryImpl implements DeploymentRepository {

    private final ProjectEntityRepository projectEntityRepository;

    @Override
    public Optional<Deployment> findDeployment(final String deploymentId) {
        final Long longDeploymentId = TypeConverter.string2Long(deploymentId);

        Optional<ProjectEntity> projectEntityOptional = projectEntityRepository.findByDeploymentId(longDeploymentId);

        return projectEntityOptional
                .map(ProjectEntity::toProject)
                .map(Project::getDeployment);
    }

    @Override
    public void setDeploymentStatus(final String deploymentId, final DeploymentStatus deploymentStatus) {
        final Long longDeploymentId = TypeConverter.string2Long(deploymentId);

        ProjectEntity projectEntity = projectEntityRepository.findByDeploymentId(longDeploymentId)
                .orElseThrow(() -> new ItemNotFoundException("Project not found"));

        projectEntity.setDeploymentStatus(deploymentStatus);

        projectEntityRepository.save(projectEntity);
    }

    @Override
    public void setDeploymentScaleAndStatus(String deploymentId, Integer deploymentScale, DeploymentStatus deploymentStatus) {
        final Long longDeploymentId = TypeConverter.string2Long(deploymentId);

        ProjectEntity projectEntity = projectEntityRepository.findByDeploymentId(longDeploymentId)
                .orElseThrow(() -> new ItemNotFoundException("Project not found"));

        projectEntity.setDeploymentStatus(deploymentStatus);
        projectEntity.setScaleFactor(deploymentScale);

        projectEntityRepository.save(projectEntity);
    }

    @Override
    public void setDeploymentStatusByProjectId(String projectId, DeploymentStatus deploymentStatus) {
        final Long longProjectId = TypeConverter.string2Long(projectId);

        ProjectEntity projectEntity = projectEntityRepository.findById(longProjectId)
                .orElseThrow(() -> new ItemNotFoundException("Project not found"));

        projectEntity.setDeploymentStatus(deploymentStatus);

        projectEntityRepository.save(projectEntity);
    }
}
