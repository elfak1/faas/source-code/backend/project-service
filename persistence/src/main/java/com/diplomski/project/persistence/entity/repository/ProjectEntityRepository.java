package com.diplomski.project.persistence.entity.repository;

import com.diplomski.project.persistence.entity.model.ProjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface ProjectEntityRepository extends JpaRepository<ProjectEntity, Long> {

    @Query(nativeQuery = true, value = "select nextval('deployment_id_seq')")
    Long getNextDeploymentId();

    boolean existsByProfileIdAndName(final Long profileId, final String name);

    @Query(value = "from ProjectEntity pr where pr.deploymentId = :deploymentId ")
    Optional<ProjectEntity> findByDeploymentId(final Long deploymentId);

    @Query(value = "from ProjectEntity pr where pr.profileId = :profileId ")
    Page<ProjectEntity> findByProjectOwner(final Long profileId, final Pageable pageable);
}
