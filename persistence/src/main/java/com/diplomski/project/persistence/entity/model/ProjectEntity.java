package com.diplomski.project.persistence.entity.model;

import com.diplomski.project.business.model.Dependency;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.model.value.DeploymentStatus;
import com.diplomski.project.business.model.value.ProgrammingLanguage;
import com.diplomski.project.business.model.UserCode;
import com.diplomski.project.persistence.util.TypeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;


@Entity
@Data
@Table(name = "project")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ProjectEntity extends BaseTimestampedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "project_id_gen")
    @SequenceGenerator(name = "project_id_gen", sequenceName = "project_id_seq", allocationSize = 1)
    private Long id;

    private Long profileId;

    private String name;

    private String description;

    private String code;

    @Enumerated(EnumType.STRING)
    private ProgrammingLanguage programmingLanguage;

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deployment_id_gen")
    @SequenceGenerator(name = "deployment_id_gen", sequenceName = "deployment_id_seq", allocationSize = 1)
    private Long deploymentId;

    private String repositoryUrl;

    private String externalId;

    private String pipelineTriggerToken;

    private Integer scaleFactor = 1;

    @Enumerated(EnumType.STRING)
    private DeploymentStatus deploymentStatus;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name="project_id", nullable=false)
    private List<DependencyEntity> dependencyEntities;

    public ProjectEntity(final Project project) {
        this.id = TypeConverter.string2Long(project.getId());
        this.profileId =  TypeConverter.string2Long(project.getProjectOwnerProfileId());
        this.name = project.getName();
        this.description = project.getDescription();
        this.code = project.getUserCode() != null ? project.getUserCode().getCode() : null;
        this.programmingLanguage =  project.getUserCode() != null ? project.getUserCode().getProgrammingLanguage() : null;
        //this.deploymentId = project.getDeployment() != null ? TypeConverter.string2Long(project.getDeployment().getId()) : null;
        this.repositoryUrl = project.getDeployment() != null ? project.getDeployment().getRepositoryUrl() : null;
        this.externalId = project.getDeployment() != null ? project.getDeployment().getExternalId() : null;
        this.pipelineTriggerToken = project.getDeployment() != null ? project.getDeployment().getPipelineTriggerToken() : null;
        this.deploymentStatus = project.getDeployment() != null ? project.getDeployment().getDeploymentStatus() : null;
        this.scaleFactor = project.getDeployment() != null ? project.getDeployment().getScaleFactor() : null;
        this.scaleFactor = 1;
        this.dependencyEntities = project.getUserCode().getDependencies().stream()
                .map(DependencyEntity::new)
                .collect(Collectors.toList());
    }

    public Project toProject() {
        final List<Dependency> dependencies = this.dependencyEntities.stream()
                .map(DependencyEntity::toDependency)
                .collect(Collectors.toList());

        final UserCode  userCode = new UserCode(
                this.getCode(),
                this.getProgrammingLanguage(),
                dependencies);

        final Deployment deployment = this.deploymentId != null ?
                new Deployment(
                        this.deploymentId.toString(),
                        this.repositoryUrl,
                        this.externalId,
                        this.pipelineTriggerToken,
                        this.deploymentStatus,
                        this.scaleFactor) :
                null;

        return new Project(
                this.getId().toString(),
                this.getName(),
                this.getDescription(),
                this.getProfileId().toString(),
                userCode,
                deployment);
    }
}
