package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.model.value.DeploymentStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ProjectStatusUpdatedMessageDto {

    private final String projectId;

    private final DeploymentStatus deploymentStatus;
}
