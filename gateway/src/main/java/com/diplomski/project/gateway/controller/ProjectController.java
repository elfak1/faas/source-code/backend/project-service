package com.diplomski.project.gateway.controller;

import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.*;
import com.diplomski.project.business.boundary.usecase.response.CreateProjectUseCaseResponse;
import com.diplomski.project.business.boundary.usecase.response.GetProfileProjectsUseCaseResponse;
import com.diplomski.project.business.boundary.usecase.response.RunProjectUseCaseResponse;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.gateway.dto.CreateProjectRequestDto;
import com.diplomski.project.gateway.dto.CreateProjectResponseDto;
import com.diplomski.project.gateway.dto.ProjectDto;
import com.diplomski.project.gateway.dto.RunProjectResponseDTO;
import com.diplomski.project.gateway.dto.response.GenericDataResponse;
import com.diplomski.project.gateway.dto.response.SimplePageResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.diplomski.project.gateway.constants.ControllerConstants.*;

@RestController
@RequiredArgsConstructor
public class ProjectController {

    private final UseCase<CreateProjectUseCaseRequest, CreateProjectUseCaseResponse> createProjectUseCase;
    private final UseCase<RunProjectUseCaseRequest, RunProjectUseCaseResponse> runProjectUseCase;
    private final UseCase<StopProjectUseCaseRequest, Void> stopProjectUseCase;
    private final UseCase<NotifyProjectRunningUseCaseRequest, Void> notifyProjectRunningUseCase;
    private final UseCase<GetProfileProjectsUseCaseRequest, GetProfileProjectsUseCaseResponse> getProfileProjectsUseCase;
    private final UseCase<DeleteProjectUseCaseRequest, Void> deleteProjectUseCase;

    @PostMapping(CREATE_PROJECT)
    public GenericDataResponse<CreateProjectResponseDto> createProject(
            @RequestHeader(PROFILE_ID_HEADER_NAME) String profileId,
            @RequestBody @Valid final CreateProjectRequestDto createProjectRequestDto) {
        final CreateProjectUseCaseRequest createProjectUseCaseRequest = createProjectRequestDto.toCreateProjectRequest(profileId);
        final CreateProjectUseCaseResponse response = createProjectUseCase.execute(createProjectUseCaseRequest);

        return new GenericDataResponse<>(new CreateProjectResponseDto(response));
    }


    @PutMapping(RUN_PROJECT)
    public GenericDataResponse<RunProjectResponseDTO> runProject(
            @RequestHeader(PROFILE_ID_HEADER_NAME) String profileId,
            @PathVariable final String projectId) {
        final RunProjectUseCaseRequest runProjectRequest = RunProjectUseCaseRequest.builder()
                .projectId(projectId)
                .profileId(profileId)
                .build();

        final RunProjectUseCaseResponse runProjectUseCaseResponse = runProjectUseCase.execute(runProjectRequest);
        final Project project = runProjectUseCaseResponse.getProject();

        return new GenericDataResponse<>(new RunProjectResponseDTO(project));
    }

    @PutMapping(STOP_PROJECT)
    public GenericDataResponse<String> stopProject(
            @RequestHeader(PROFILE_ID_HEADER_NAME) String profileId,
            @PathVariable final String projectId) {
        final StopProjectUseCaseRequest stopProjectRequest = StopProjectUseCaseRequest.builder()
                .projectId(projectId)
                .profileId(profileId)
                .build();

        stopProjectUseCase.execute(stopProjectRequest);

        return new GenericDataResponse<>("Success!");
    }

    @PutMapping(PROJECT_RUNNING)
    public GenericDataResponse<String> notifyProjectRunning(
            @PathVariable final String projectId) {
        final NotifyProjectRunningUseCaseRequest notifyProjectRunningUseCaseRequest = NotifyProjectRunningUseCaseRequest.builder()
                .projectId(projectId)
                .build();

        notifyProjectRunningUseCase.execute(notifyProjectRunningUseCaseRequest);

        return new GenericDataResponse<>("Success!");
    }

    @GetMapping(GET_PROJECTS)
    public GenericDataResponse<SimplePageResponse<ProjectDto>> getProjects(
            @RequestHeader(PROFILE_ID_HEADER_NAME) String profileId,
            @PageableDefault final Pageable pageable) {
        final GetProfileProjectsUseCaseRequest getProfileProjectsUseCaseRequest =
                new GetProfileProjectsUseCaseRequest(profileId, pageable);

        final GetProfileProjectsUseCaseResponse getProfileProjectsUseCaseResponse = getProfileProjectsUseCase
                .execute(getProfileProjectsUseCaseRequest);

        final Page<ProjectDto> projectPage = getProfileProjectsUseCaseResponse.getProjectPage()
                .map(ProjectDto::new);

        return new GenericDataResponse<>(new SimplePageResponse<>(projectPage));
    }

    @DeleteMapping(DELETE_PROJECT)
    public GenericDataResponse<String> deleteProject(
            @PathVariable final String projectId,
            @RequestHeader(PROFILE_ID_HEADER_NAME) String profileId) {
        final DeleteProjectUseCaseRequest deleteProjectUseCaseRequest =
                new DeleteProjectUseCaseRequest(projectId, profileId);

        deleteProjectUseCase.execute(deleteProjectUseCaseRequest);

        return new GenericDataResponse<>("Project successfully deleted");
    }
}
