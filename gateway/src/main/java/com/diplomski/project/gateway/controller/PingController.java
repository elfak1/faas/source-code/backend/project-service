package com.diplomski.project.gateway.controller;

import com.diplomski.project.gateway.dto.response.GenericDataResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.diplomski.project.gateway.constants.ControllerConstants.PING;

@RestController
public class PingController {

    @GetMapping(PING)
    public GenericDataResponse<String> ping() {
        return new GenericDataResponse<>("Pong");
    }
}
