package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.model.UserCode;
import com.diplomski.project.business.model.value.ProgrammingLanguage;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class UserCodeDto {

    private final String code;

    private final ProgrammingLanguage programmingLanguage;

    private final List<DependencyDto> dependencies;

    public UserCodeDto(final UserCode userCode) {
        this.code = userCode.getCode();
        this.programmingLanguage = userCode.getProgrammingLanguage();
        this.dependencies = userCode.getDependencies().stream()
                .map(DependencyDto::new)
                .collect(Collectors.toList());
    }
}
