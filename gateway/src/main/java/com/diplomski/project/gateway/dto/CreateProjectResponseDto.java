package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.boundary.usecase.response.CreateProjectUseCaseResponse;
import lombok.Getter;

@Getter
public class CreateProjectResponseDto {

    private final String projectName;

    private final String executeEndpoint;

    private final String codeInfoEndpoint;

    public CreateProjectResponseDto(final CreateProjectUseCaseResponse response) {
        this.projectName = response.getProject().getName();
        this.executeEndpoint = response.getProject().getExecuteEndpoint();
        this.codeInfoEndpoint = response.getProject().getCodeInfoEndpoint();
    }
}
