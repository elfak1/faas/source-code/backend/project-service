package com.diplomski.project.gateway.dto.response;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@EqualsAndHashCode
public class SimplePageResponse<T> {

    @NotNull
    private final List<T> content;

    @NotNull
    private final int page;

    @NotNull
    private final int size;

    @NotNull
    private final long totalItems;

    @NotNull
    private final int totalPages;

    @NotNull
    private final boolean first;

    @NotNull
    private final boolean last;

    public SimplePageResponse(Page<T> page) {
        this.content = page.getContent();
        this.page = page.getPageable().getPageNumber();
        this.size = page.getPageable().getPageSize();
        this.totalItems = page.getTotalElements();
        this.totalPages = page.getTotalPages();
        this.first = page.isFirst();
        this.last = page.isLast();
    }

    public SimplePageResponse(Slice<T> slice) {
        this.content = slice.getContent();
        this.page = slice.getPageable().getPageNumber();
        this.size = slice.getPageable().getPageSize();
        this.totalItems = -1;
        this.totalPages = -1;
        this.first = slice.isFirst();
        this.last = slice.isLast();
    }
}
