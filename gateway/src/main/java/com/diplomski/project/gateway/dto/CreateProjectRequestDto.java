package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.boundary.usecase.request.CreateDependencyUseCaseRequest;
import com.diplomski.project.business.boundary.usecase.request.CreateProjectUseCaseRequest;
import com.diplomski.project.business.model.value.ProgrammingLanguage;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@JsonDeserialize(builder = CreateProjectRequestDto.CreateProjectRequestDtoBuilder.class)
@Getter
@Builder(builderClassName = "CreateProjectRequestDtoBuilder", setterPrefix = "with")
public class CreateProjectRequestDto {

    @NotNull
    private final ProgrammingLanguage language;

    @NotNull
    @NotBlank
    private final String projectName;

    @NotNull
    @NotBlank
    private final String userCode;

    @JsonProperty("dependencies")
    private final List<CreateDependencyRequestDto> createDependencyRequestDtoList;


    public CreateProjectUseCaseRequest toCreateProjectRequest(final String profileId) {
        final List<CreateDependencyUseCaseRequest> createDependencyUseCaseRequests = Optional.ofNullable(createDependencyRequestDtoList)
                .orElse(Collections.emptyList()).stream()
                .map(CreateDependencyRequestDto::toCreateDependencyRequest)
                .collect(Collectors.toList());

        return CreateProjectUseCaseRequest.builder()
                .profileId(profileId)
                .userCode(this.getUserCode())
                .projectName(this.getProjectName())
                .language(this.getLanguage())
                .createDependencyUseCaseRequests(createDependencyUseCaseRequests)
                .build();
    }
}
