package com.diplomski.project.gateway.constants;

public class ControllerConstants {

    public static final String BASE_API = "/api/project" ;
    public static final String PUBLIC_BASE = BASE_API + "/public";
    public static final String PRIVATE_BASE = BASE_API + "/private";

    public static final String CREATE_PROJECT = BASE_API;
    public static final String GET_PROJECTS = BASE_API;
    public static final String DELETE_PROJECT = BASE_API + "/{projectId}";
    public static final String RUN_PROJECT = BASE_API + "/{projectId}/run";
    public static final String STOP_PROJECT = BASE_API + "/{projectId}/stop";
    public static final String PROJECT_RUNNING = PRIVATE_BASE + "/{projectId}/running";
    public static final String PING = PUBLIC_BASE + "/ping";

    public static final String PROFILE_ID_HEADER_NAME = "x-jwt-sub-id";
}
