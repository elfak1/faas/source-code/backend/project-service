package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.boundary.usecase.request.CreateDependencyUseCaseRequest;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;

@JsonDeserialize(builder = CreateDependencyRequestDto.CreateDependencyRequestDtoBuilder.class)
@Getter
@Builder(builderClassName = "CreateDependencyRequestDtoBuilder", setterPrefix = "with")
public class CreateDependencyRequestDto {

    private final String name;

    private final String value;

    public CreateDependencyUseCaseRequest toCreateDependencyRequest() {
        return CreateDependencyUseCaseRequest.builder()
                .name(this.name)
                .value(this.value)
                .build();
    }
}
