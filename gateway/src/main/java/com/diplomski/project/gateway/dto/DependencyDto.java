package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.model.Dependency;
import lombok.Getter;

@Getter
public class DependencyDto {

    private final String name;

    private final String value;

    public DependencyDto(final Dependency dependency) {
        this.name = dependency.getName();
        this.value = dependency.getValue();
    }
}
