package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.model.Project;
import lombok.Getter;

@Getter
public class RunProjectResponseDTO {

    private final String projectName;

    private final String executeEndpoint;

    private final String codeInfoEndpoint;

    public RunProjectResponseDTO(final Project project) {
        this.projectName = project.getName();
        this.executeEndpoint = project.getExecuteEndpoint();
        this.codeInfoEndpoint = project.getCodeInfoEndpoint();
    }
}
