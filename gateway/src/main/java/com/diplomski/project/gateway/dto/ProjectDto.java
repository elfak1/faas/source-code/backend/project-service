package com.diplomski.project.gateway.dto;

import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.model.value.DeploymentStatus;
import com.diplomski.project.business.model.value.ProgrammingLanguage;
import lombok.Getter;

@Getter
public class ProjectDto {

    private final String id;

    private final String name;

    private final String description;

    private final String projectOwnerProfileId;

    private final ProgrammingLanguage programmingLanguage;

    private final DeploymentStatus deploymentStatus;

    private final String executeEndpoint;

    private final String codeInfoEndpoint;

    private final UserCodeDto userCode;

    public ProjectDto(final Project project) {
        this.id = project.getId();
        this.name = project.getName();
        this.description = project.getDescription();
        this.projectOwnerProfileId = project.getProjectOwnerProfileId();
        this.programmingLanguage = project.getUserCode() != null ? project.getUserCode().getProgrammingLanguage() : null;
        this.deploymentStatus = project.getDeployment() != null ? project.getDeployment().getDeploymentStatus() : null;
        this.userCode = new UserCodeDto(project.getUserCode());
        this.executeEndpoint = project.getExecuteEndpoint();
        this.codeInfoEndpoint = project.getCodeInfoEndpoint();
    }
}
