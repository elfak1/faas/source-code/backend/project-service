package com.diplomski.project.gateway.service;

import com.diplomski.project.business.boundary.gateway.NotificationService;
import com.diplomski.project.business.model.value.DeploymentStatus;
import com.diplomski.project.gateway.dto.ProjectStatusUpdatedMessageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {

    private final SimpMessagingTemplate messagingTemplate;

    @Override
    public void sendDeploymentRunningMessage(String projectId) {
        final ProjectStatusUpdatedMessageDto projectStatusUpdatedMessageDto =
                new ProjectStatusUpdatedMessageDto(projectId, DeploymentStatus.STARTING);

        messagingTemplate.convertAndSend("/topic", projectStatusUpdatedMessageDto);
    }
}
