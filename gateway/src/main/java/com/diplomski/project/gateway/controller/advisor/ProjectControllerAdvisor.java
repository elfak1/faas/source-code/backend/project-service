package com.diplomski.project.gateway.controller.advisor;

import com.diplomski.project.business.exception.*;
import com.diplomski.project.gateway.dto.response.BaseResponse;
import com.diplomski.project.gateway.dto.response.GenericListResponse;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice(basePackages = {"com.diplomski.project.gateway.controller"})
public class ProjectControllerAdvisor extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        final List<FieldValidationError> fieldValidationErrors = exception.getBindingResult().getFieldErrors().stream()
                .map(fieldError -> new FieldValidationError(
                        fieldError.getField(),
                        StringUtils.capitalize(fieldError.getDefaultMessage())))
                .collect(Collectors.toList());

        final GenericListResponse<FieldValidationError> genericListResponse = new GenericListResponse<>(
                fieldValidationErrors,
                HttpStatus.BAD_REQUEST.value(),
                "Invalid request parameter(s)",
                HttpStatus.BAD_REQUEST.getReasonPhrase());

        return new ResponseEntity<>(
                genericListResponse,
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {
            ProjectAlreadyExistsException.class,
            DeploymentNotRunningException.class,
            DeploymentAlreadyRunningException.class
    })
    public ResponseEntity<BaseResponse> handleConflictExceptions(RuntimeException exception, WebRequest req) {
        return new ResponseEntity<>(
                new BaseResponse(HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT.getReasonPhrase(), exception.getMessage()),
                HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {
            UnauthorizedToRunProjectException.class,
            UnauthorizedToDeleteProjectException.class
    })
    public ResponseEntity<BaseResponse> handleForbiddenExceptions(RuntimeException exception, WebRequest req) {
        return new ResponseEntity<>(
                new BaseResponse(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.getReasonPhrase(), exception.getMessage()),
                HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = {
            ItemNotFoundException.class,
    })
    public ResponseEntity<BaseResponse> handleNotFoundExceptions(RuntimeException exception, WebRequest req) {
        return new ResponseEntity<>(
                new BaseResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase(), exception.getMessage()),
                HttpStatus.NOT_FOUND);
    }

    @Getter
    @RequiredArgsConstructor
    public static final class FieldValidationError {

        private final String fieldName;

        private final String message;
    }
}