package com.diplomski.project.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static com.diplomski.project.gateway.constants.ControllerConstants.*;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().and().csrf().disable().authorizeRequests()
        .antMatchers(CREATE_PROJECT).permitAll()
        .antMatchers(GET_PROJECTS).permitAll()
        .antMatchers(DELETE_PROJECT).permitAll()
        .antMatchers(RUN_PROJECT).permitAll()
        .antMatchers(STOP_PROJECT).permitAll()
        .antMatchers(PROJECT_RUNNING).permitAll()
        .antMatchers("/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/v2/api-docs/**",
                "/swagger-ui/**",
                "/webjars/**").permitAll()
        .anyRequest().authenticated();
    }
}
