CREATE TABLE project
(
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id bigint NOT NULL,
	group_id bigint NOT NULL,
	name character varying(1024),
    description character varying(1024),
	code character varying,
	programing_language character varying(1024),
	repository_id character varying(1024),
	repository_url character varying(1024),
    CONSTRAINT project_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;