CREATE TABLE dependency
(
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    id bigint NOT NULL,
	name character varying(1024),
    dependency_value character varying(1024),
    project_id bigint NOT NULL,
    CONSTRAINT dependency_pky PRIMARY KEY (id)
);

CREATE SEQUENCE dependency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;