alter table project drop column programing_language;
alter table project add column external_id character varying(1024);
alter table project add column pipeline_trigger_token character varying(1024);
alter table project add column deployment_status character varying(1024);