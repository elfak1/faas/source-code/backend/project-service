ALTER TABLE project
RENAME COLUMN repository_id TO deployment_id;


update project set deployment_id = id;

CREATE SEQUENCE deployment_id_seq
    START 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SELECT setval('deployment_id_seq', (SELECT last_value FROM project_id_seq), true);



