package com.diplomski.project.git;

import com.diplomski.project.business.boundary.git.GitService;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.value.DeploymentStatus;
import com.diplomski.project.git.adapter.GitLabApiAdapter;
import com.diplomski.project.git.configuration.Constants;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.gitlab4j.api.models.CommitAction;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.Trigger;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GitLabService implements GitService {

    private final GitLabApiAdapter gitLabApiAdapter;
    private final CommitService commitService;

    @Override
    @SneakyThrows
    public Deployment createAndRunDeployment(final com.diplomski.project.business.model.Project project) {
        Project gitLabProject = gitLabApiAdapter.createGitLabProject(project.getId(), project.getId());

        //Since the project is not always created after the createGitLabProject function exits,
        //we need to wait some time before applying any commits to the project
        Thread.sleep(3 * 1000);

        final List<CommitAction> commitActions = commitService.createInitialCommitActions(project);

        gitLabApiAdapter.crateCommit(gitLabProject.getId(), commitActions);

        final Trigger pipelineTrigger = gitLabApiAdapter.createPipelineTrigger(gitLabProject.getId());

        return new Deployment(
                createRepositoryUrl(gitLabProject),
                gitLabProject.getId().toString(),
                pipelineTrigger.getToken(),
                DeploymentStatus.STARTING,
                1
        );
    }

    @Override
    public void deleteDeployment(Deployment deployment) {
        final Integer gitLabProjectId = Integer.parseInt(deployment.getExternalId());

        gitLabApiAdapter.triggerDeletePipeline(
                gitLabProjectId,
                deployment.getPipelineTriggerToken());
    }

    @Override
    public void runDeployment(final Deployment deployment) {
        scaleDeployment(deployment, 1);
    }

    @Override
    public void stopDeployment(final Deployment deployment) {
        scaleDeployment(deployment, 0);
    }

    @Override
    public void scaleDeployment(final Deployment deployment, final Integer scaleFactor) {
        final Integer gitLabProjectId = Integer.parseInt(deployment.getExternalId());

        gitLabApiAdapter.triggerScalePipeline(
                gitLabProjectId,
                deployment.getPipelineTriggerToken(),
                scaleFactor);
    }

    private String createRepositoryUrl(Project gitProject) {
        return Constants.GIT_SSH_BASE_SERVER + ":" + gitProject.getPathWithNamespace() + ".git";
    }
}
