package com.diplomski.project.git.configuration;

import org.gitlab4j.api.GitLabApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.diplomski.project.git.configuration.Constants.ACCESS_TOKEN;
import static com.diplomski.project.git.configuration.Constants.GIT_BASE_SERVER;

@Configuration
public class GitConfig {

    @Bean
    public GitLabApi gitLabApi() {
        return new GitLabApi(GIT_BASE_SERVER, ACCESS_TOKEN);
    }
}
