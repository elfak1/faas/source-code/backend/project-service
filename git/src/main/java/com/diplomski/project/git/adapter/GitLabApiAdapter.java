package com.diplomski.project.git.adapter;

import com.diplomski.project.git.exception.UnhandledGitLabException;
import lombok.RequiredArgsConstructor;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.CommitAction;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.Trigger;
import org.gitlab4j.api.models.Variable;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.diplomski.project.git.configuration.Constants.*;

@Component
@RequiredArgsConstructor
public class GitLabApiAdapter {

    private final GitLabApi gitLabApi;

    public Project createGitLabProject(final String projectName, final String projectPath) {
        return handleGitlabException(() -> gitLabApi.getProjectApi()
                .forkProject(
                        WRAPPER_SERVICE_PROJECT_ID,
                        USER_PROJECTS_GROUP_ID,
                        projectName,
                        projectName));
    }

    public void crateCommit(final Integer gitLabProjectId, List<CommitAction> commitActions) {
        handleGitlabException(() -> {
            gitLabApi.getCommitsApi().createCommit(
                    gitLabProjectId,
                    DEFAULT_BRANCH_NAME,
                    INITIAL_COMMIT_MESSAGE,
                    DEFAULT_BRANCH_NAME,
                    COMMIT_AUTHOR_EMAIL,
                    COMMIT_AUTHOR_NAME,
                    commitActions);
            return null;
        });
    }

    public Trigger createPipelineTrigger(final Integer gitLabProjectId) {
        return handleGitlabException(() ->
                gitLabApi.getPipelineApi()
                        .createPipelineTrigger(gitLabProjectId, PIPELINE_TRIGGER_DESCRIPTION));
    }

    public void triggerScalePipeline(
            final Integer gitLabProjectId,
            final String pipelineTriggerToken,
            final Integer scaleFactor ) {
        handleGitlabException(() -> {

            final List<Variable> variables = List.of(
                    new Variable(PIPELINE_CI_ACTION_HEADER_NAME, PIPELINE_CI_ACTION_HEADER_SCALE_VALUE),
                    new Variable(PIPELINE_SCALE_FACTOR_HEADER_NAME, scaleFactor.toString())
            );

            triggerPipeline(gitLabProjectId, pipelineTriggerToken, variables);

            return null;
        });
    }

    public void triggerDeletePipeline(
            final Integer gitLabProjectId,
            final String pipelineTriggerToken) {
        handleGitlabException(() -> {

            final List<Variable> variables = List.of(
                    new Variable(PIPELINE_CI_ACTION_HEADER_NAME, PIPELINE_CI_ACTION_HEADER_DELETE_VALUE)
            );

            triggerPipeline(gitLabProjectId, pipelineTriggerToken, variables);

            return null;
        });
    }

    private void triggerPipeline(
            final Integer gitLabProjectId,
            final String pipelineTriggerToken,
            final List<Variable> variables) throws GitLabApiException {
        gitLabApi.getPipelineApi().triggerPipeline(
                gitLabProjectId,
                pipelineTriggerToken,
                DEFAULT_BRANCH_NAME,
                variables);
    }

    private <T, E extends Exception> T handleGitlabException(ExFunction<T, E> function) {
        try {
            return function.apply();
        } catch (Exception exception) {
            throw new UnhandledGitLabException(exception.getMessage());
        }
    }

    @FunctionalInterface
    public interface ExFunction<T, E extends Exception> {
        T apply() throws E;
    }

}
