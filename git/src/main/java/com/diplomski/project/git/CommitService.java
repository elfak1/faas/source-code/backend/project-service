package com.diplomski.project.git;

import com.diplomski.project.business.model.Dependency;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.model.UserCode;
import org.gitlab4j.api.models.CommitAction;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.diplomski.project.git.configuration.CommitConstants.*;

@Component
public class CommitService {

    public List<CommitAction> createInitialCommitActions(final Project project) {
        return List.of(
                createCommitAction(CommitAction.Action.UPDATE, project.getUserCode().getCode(), USER_CODE_FILE_LOCATION),
                createCommitAction(CommitAction.Action.UPDATE, createPomFileContents(project.getUserCode()), POM_FILE_LOCATION),
                createCommitAction(CommitAction.Action.UPDATE, createInjectedConstantsFile(project), INJECTED_CONSTANTS_FILE_LOCATION),
                createCommitAction(CommitAction.Action.CREATE, GIT_LAB_CI_YAML_CONTENT, GIT_LAB_CI_YAML_LOCATION)
        );
    }

    private CommitAction createCommitAction(CommitAction.Action commitAction, String fileContent, String filePath) {
        CommitAction CreateUserCodeCommitAction = new CommitAction();
        CreateUserCodeCommitAction.setAction(commitAction);
        CreateUserCodeCommitAction.withContent(fileContent);
        CreateUserCodeCommitAction.setFilePath(filePath);
        return CreateUserCodeCommitAction;
    }

    private String createPomFileContents(final UserCode userCode) {
        final String dependencies = userCode.getDependencies().stream()
                .map(Dependency::getValue)
                .collect(Collectors.joining("\n"));

        return POM_FILE_CONTENT.replace(DEPENDENCY_PLACEHOLDER, dependencies);
    }

    private String createInjectedConstantsFile(final Project project) {
        return String.format(INJECTED_CONSTANTS_FILE_CONTENT, project.getId());
    }
}
