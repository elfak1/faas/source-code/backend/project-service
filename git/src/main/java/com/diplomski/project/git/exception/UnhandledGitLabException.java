package com.diplomski.project.git.exception;

public class UnhandledGitLabException extends RuntimeException {

    public UnhandledGitLabException(final String message) {
        super(message);
    }
}