package com.diplomski.project.git.configuration;

public class CommitConstants {


    public static final String GIT_LAB_CI_YAML_LOCATION = ".gitlab-ci.yml";
    public static final String POM_FILE_LOCATION = "pom.xml";
    public static final String USER_CODE_FILE_LOCATION = "src/main/resources/UserCode.java";
    public static final String INJECTED_CONSTANTS_FILE_LOCATION = "src/main/java/com/Diplomski/WrapperService/Configurations/InjectedConstants.java";
    public static final String DEPENDENCY_PLACEHOLDER = "<!-- Placeholder for third party dependencies -->";

    public static final String INJECTED_CONSTANTS_FILE_CONTENT = "package com.Diplomski.WrapperService.Configurations;\n" +
            "\n" +
            "public class InjectedConstants {\n" +
            "    public static final String PROJECT_ID = \"%s\";\n" +
            "}";

    public static final String GIT_LAB_CI_YAML_CONTENT =
            "stages:\n" +
                    "  - build\n" +
                    "  - dockerize\n" +
                    "  - publish\n" +
                    "  - delete\n" +
                    "  - scale\n" +
                    "\n" +
                    "\n" +
                    "image: docker:latest\n" +
                    "services:\n" +
                    "  - docker:18.09.7-dind\n" +
                    "variables:\n" +
                    "  MAVEN_OPTS: \"-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository\"\n" +
                    " \n" +
                    "\n" +
                    "cache:\n" +
                    "  paths:\n" +
                    "    - .m2/repository\n" +
                    "  key: \"$CI_BUILD_REF_NAME\"\n" +
                    "\n" +
                    "build:\n" +
                    "  rules: # whether to include the job in the pipeline\n" +
                    "  - if: '$CI_PIPELINE_SOURCE == \"trigger\"'\n" +
                    "    when: never\n" +
                    "  - when: on_success  \n" +
                    "  image: maven:3-jdk-8\n" +
                    "  stage: build\n" +
                    "  tags:\n" +
                    "    - kubernetes\n" +
                    "  script:\n" +
                    "    - mvn clean package\n" +
                    "  artifacts:\n" +
                    "    paths:\n" +
                    "      - target/*.jar\n" +
                    "\n" +
                    "dockerize:\n" +
                    "  rules: # whether to include the job in the pipeline\n" +
                    "  - if: '$CI_PIPELINE_SOURCE == \"trigger\"'\n" +
                    "    when: never\n" +
                    "  - when: on_success  \n" +
                    "  stage: dockerize\n" +
                    "  tags:\n" +
                    "    - kubernetes\n" +
                    "  before_script:\n" +
                    "    - docker login -u \"$CI_REGISTRY_USER\" -p \"$CI_REGISTRY_PASSWORD\" $CI_REGISTRY\n" +
                    "  script:\n" +
                    "    - export DOCKER_HOST=tcp://127.0.0.1:2375\n" +
                    "    - docker build --pull -t \"$CI_REGISTRY_IMAGE\" .\n" +
                    "    - docker push \"$CI_REGISTRY_IMAGE\"\n" +
                    "\n" +
                    "  \n" +
                    "\n" +
                    "deploy-k:\n" +
                    "  stage: publish\n" +
                    "  tags:\n" +
                    "    - kubernetes\n" +
                    "  image: dtzar/helm-kubectl\n" +
                    "  rules: # whether to include the job in the pipeline\n" +
                    "  - if: '$CI_PIPELINE_SOURCE == \"trigger\" && $CI_ACTION == \"deploy\"'\n" +
                    "    when: always\n" +
                    "  - if: '$CI_PIPELINE_SOURCE == \"trigger\"'\n" +
                    "    when: never\n" +
                    "  - when: on_success  \n" +
                    "  script:\n" +
                    "    - kubectl config set-cluster k8s --server=\"${SERVER}\"\n" +
                    "    - kubectl config set clusters.k8s.certificate-authority-data ${CERTIFICATE_AUTHORITY_DATA}\n" +
                    "    - kubectl config set-credentials gitlab --token=\"${USER_TOKEN}\"\n" +
                    "    - kubectl config set-context default --cluster=k8s --user=gitlab\n" +
                    "    - kubectl config use-context default\n" +
                    "    - sed -i \"s/<PROJECT_NAME>/${CI_PROJECT_NAME}/g\" ./deployment.yaml\n" +
                    "    - sed -i \"s@<PROJECT_IMAGE>@${CI_REGISTRY_IMAGE}@g\" ./deployment.yaml\n" +
                    "    - kubectl apply -f deployment.yaml\n" +
                    "  allow_failure: false\n" +
                    "\n" +
                    "delete-k:\n" +
                    "  stage: delete\n" +
                    "  rules: # whether to include the job in the pipeline and if its the delete job\n" +
                    "  - if: '$CI_PIPELINE_SOURCE == \"trigger\" && $CI_ACTION == \"delete\"'\n" +
                    "    when: always\n" +
                    "  - when: never\n" +
                    "  tags:\n" +
                    "  - kubernetes\n" +
                    "  image: dtzar/helm-kubectl\n" +
                    "  script: \n" +
                    "    - kubectl config set-cluster k8s --server=\"${SERVER}\"\n" +
                    "    - kubectl config set clusters.k8s.certificate-authority-data ${CERTIFICATE_AUTHORITY_DATA}\n" +
                    "    - kubectl config set-credentials gitlab --token=\"${USER_TOKEN}\"\n" +
                    "    - kubectl config set-context default --cluster=k8s --user=gitlab\n" +
                    "    - kubectl config use-context default\n" +
                    "    - sed -i \"s/<PROJECT_NAME>/${CI_PROJECT_NAME}/g\" ./deployment.yaml\n" +
                    "    - sed -i \"s@<PROJECT_IMAGE>@${CI_REGISTRY_IMAGE}@g\" ./deployment.yaml    \n" +
                    "    - kubectl delete -f deployment.yaml\n" +
                    "\n" +
                    "scale-k:\n" +
                    "  stage: scale\n" +
                    "  tags:\n" +
                    "    - kubernetes\n" +
                    "  image: dtzar/helm-kubectl\n" +
                    "  rules: # whether to include the job in the pipeline and if its the scale job\n" +
                    "  - if: '$CI_PIPELINE_SOURCE == \"trigger\" && $CI_ACTION == \"scale\"'\n" +
                    "    when: always\n" +
                    "  - when: never\n" +
                    "  script:\n" +
                    "    - kubectl config set-cluster k8s --server=\"${SERVER}\"\n" +
                    "    - kubectl config set clusters.k8s.certificate-authority-data ${CERTIFICATE_AUTHORITY_DATA}\n" +
                    "    - kubectl config set-credentials gitlab --token=\"${USER_TOKEN}\"\n" +
                    "    - kubectl config set-context default --cluster=k8s --user=gitlab\n" +
                    "    - kubectl config use-context default\n" +
                    "    - kubectl scale deployment/faas-${CI_PROJECT_NAME} --replicas=${CI_SCALE_REPLICAS} -n gitlab-managed-apps \n";

    public static final String POM_FILE_CONTENT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
            "\txsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd\">\n" +
            "\t<modelVersion>4.0.0</modelVersion>\n" +
            "\t<parent>\n" +
            "\t\t<groupId>org.springframework.boot</groupId>\n" +
            "\t\t<artifactId>spring-boot-starter-parent</artifactId>\n" +
            "\t\t<version>2.3.4.RELEASE</version>\n" +
            "\t\t<relativePath/> <!-- lookup parent from repository -->\n" +
            "\t</parent>\n" +
            "\t<groupId>com.Diplomski</groupId>\n" +
            "\t<artifactId>WrapperService</artifactId>\n" +
            "\t<version>0.0.1-SNAPSHOT</version>\n" +
            "\t<name>WrapperService</name>\n" +
            "\t<description>Demo project for Spring Boot</description>\n" +
            "\n" +
            "\t<properties>\n" +
            "\t\t<java.version>1.8</java.version>\n" +
            "\t</properties>\n" +
            "\n" +
            "\t<dependencies>\n" +
            "\t\t<dependency>\n" +
            "\t\t\t<groupId>org.springframework.boot</groupId>\n" +
            "\t\t\t<artifactId>spring-boot-starter-web</artifactId>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->\n" +
            "\t\t<dependency>\n" +
            "\t\t\t<groupId>commons-io</groupId>\n" +
            "\t\t\t<artifactId>commons-io</artifactId>\n" +
            "\t\t\t<version>2.6</version>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<dependency>\n" +
            "\t\t\t<groupId>net.openhft</groupId>\n" +
            "\t\t\t<artifactId>compiler</artifactId>\n" +
            "\t\t\t<version>2.3.6</version>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->\n" +
            "\t\t<dependency>\n" +
            "\t\t\t<groupId>com.google.code.gson</groupId>\n" +
            "\t\t\t<artifactId>gson</artifactId>\n" +
            "\t\t\t<version>2.8.6</version>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<dependency>\n" +
            "\t\t  <groupId>org.mdkt.compiler</groupId>\n" +
            "\t\t  <artifactId>InMemoryJavaCompiler</artifactId>\n" +
            "\t\t  <version>1.3.0</version>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<dependency>\n" +
            "\t\t\t<groupId>org.projectlombok</groupId>\n" +
            "\t\t\t<artifactId>lombok</artifactId>\n" +
            "\t\t\t<optional>true</optional>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<dependency>\n" +
            "\t\t\t<groupId>org.springframework.boot</groupId>\n" +
            "\t\t\t<artifactId>spring-boot-starter-test</artifactId>\n" +
            "\t\t\t<scope>test</scope>\n" +
            "\t\t\t<exclusions>\n" +
            "\t\t\t\t<exclusion>\n" +
            "\t\t\t\t\t<groupId>org.junit.vintage</groupId>\n" +
            "\t\t\t\t\t<artifactId>junit-vintage-engine</artifactId>\n" +
            "\t\t\t\t</exclusion>\n" +
            "\t\t\t</exclusions>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<dependency>\n" +
            "\t\t\t<groupId>junit</groupId>\n" +
            "\t\t\t<artifactId>junit</artifactId>\n" +
            "\t\t\t<scope>test</scope>\n" +
            "\t\t</dependency>\n" +
            "\n" +
            "\t\t<!-- Placeholder for third party dependencies -->\n" +
            "\t</dependencies>\n" +
            "\n" +
            "\t<build>\n" +
            "\t\t<plugins>\n" +
            "\t\t\t<plugin>\n" +
            "\t\t\t\t<groupId>org.springframework.boot</groupId>\n" +
            "\t\t\t\t<artifactId>spring-boot-maven-plugin</artifactId>\n" +
            "        <configuration>\n" +
            "          <layers>\n" +
            "            <enabled>true</enabled>\n" +
            "          </layers>\n" +
            "        </configuration>\n" +
            "\t\t\t</plugin>\n" +
            "\t\t</plugins>\n" +
            "\t</build>\n" +
            "\n" +
            "</project>\n";
}
