package com.diplomski.project.git.configuration;

public class Constants {

    // TODO: move all of these constants to environment variables variables

    //project
    public static final String USER_PROJECTS_GROUP_ID = "8750858";
    public static final String USER_PROJECTS_FULL_PATH = "elfak1/faas/testPath";
    public static final String WRAPPER_SERVICE_PROJECT_ID = "25893802";
    public static final String DEFAULT_BRANCH_NAME = "master";
    public static final String INITIAL_COMMIT_MESSAGE = "adding user code file";

    //pipeline
    public static final String PIPELINE_TRIGGER_DESCRIPTION = "main project trigger";
    public static final String PIPELINE_SCALE_FACTOR_HEADER_NAME = "CI_SCALE_REPLICAS";
    public static final String PIPELINE_CI_ACTION_HEADER_NAME = "CI_ACTION";
    public static final String PIPELINE_CI_ACTION_HEADER_SCALE_VALUE = "scale";
    public static final String PIPELINE_CI_ACTION_HEADER_DELETE_VALUE = "delete";

    //credentials
    public static final String ACCESS_TOKEN = "glpat-9k3nQy-8fbSnrhpxv2tY";
    public static final String COMMIT_AUTHOR_EMAIL = "bogosavljevic.1911@gmail.com";
    public static final String COMMIT_AUTHOR_NAME = "stefan";
    public static final String GIT_BASE_SERVER = "https://gitlab.com";
    public static final String GIT_SSH_BASE_SERVER = "git@gitlab.com";
}
