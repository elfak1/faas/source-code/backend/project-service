package com.diplomski.project.business.boundary.repository.response;

import com.diplomski.project.business.model.Project;
import lombok.Builder;
import lombok.Getter;

import java.util.Optional;

@Getter
@Builder
public class FindProjectRepoResponse {

    private final Optional<Project> projectOptional;
}
