package com.diplomski.project.business.usecase;

import com.diplomski.project.business.boundary.git.GitService;
import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.StopProjectUseCaseRequest;
import com.diplomski.project.business.exception.DeploymentNotRunningException;
import com.diplomski.project.business.exception.UnauthorizedToRunProjectException;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.services.deployment.StopDeploymentService;
import com.diplomski.project.business.services.project.GetProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StopProjectUseCase implements UseCase<StopProjectUseCaseRequest, Void> {

    private final GitService gitService;
    private final GetProjectService getProjectService;
    private final StopDeploymentService stopDeploymentService;

    @Override
    public Void execute(StopProjectUseCaseRequest useCaseRequest) {
        final Project project = getProjectService.getProject(useCaseRequest.getProjectId());
        final Deployment deployment = project.getDeployment();

        if (!project.getProjectOwnerProfileId().equals(useCaseRequest.getProfileId())) {
            throw new UnauthorizedToRunProjectException();
        }

        if (!deployment.getDeploymentStatus().isStoppableStatus()) {
            throw new DeploymentNotRunningException(project);
        }

        stopDeploymentService.stopDeployment(project.getDeployment());

        return null;
    }
}
