package com.diplomski.project.business.boundary.repository.request;

import com.diplomski.project.business.model.value.DeploymentStatus;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SetDeploymentStatusRepoRequest {

    private final String deploymentId;

    private final DeploymentStatus deploymentStatus;
}
