package com.diplomski.project.business.boundary.repository;

import com.diplomski.project.business.boundary.repository.request.FindProjectRepoRequest;
import com.diplomski.project.business.boundary.repository.response.FindProjectRepoResponse;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProjectRepository {

    Project saveProject(final Project project);

    void deleteProjectById(final String projectId);

    Project setProjectDeployment(final String projectId, final Deployment deployment);

    boolean existsByProjectNameAndProfileId(final String projectName, final String profileId);

    FindProjectRepoResponse findProject(final FindProjectRepoRequest findProjectRepoRequest);

    Page<Project> findByProjectOwner(final String projectOwnerId, Pageable pageable);

}