package com.diplomski.project.business.services.deployment;

import com.diplomski.project.business.boundary.git.GitService;
import com.diplomski.project.business.boundary.repository.DeploymentRepository;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.value.DeploymentStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ScaleDeploymentService {

    private final GitService gitService;
    final DeploymentRepository deploymentRepository;
    private final DeploymentFailureService deploymentFailureService;

    @Transactional
    public void scaleDeployment(final Deployment deployment, final Integer deploymentScale) {

        deploymentRepository.setDeploymentScaleAndStatus(deployment.getId(), deploymentScale, DeploymentStatus.STARTING);

        gitService.scaleDeployment(deployment, deploymentScale);

        deploymentFailureService.checkDeploymentFailureAfterTimeout(deployment.getId());
    }
}
