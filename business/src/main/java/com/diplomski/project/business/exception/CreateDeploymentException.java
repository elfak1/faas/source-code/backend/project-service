package com.diplomski.project.business.exception;

public class CreateDeploymentException extends RuntimeException {

    public CreateDeploymentException(Throwable cause) {
        super(cause);
    }
}
