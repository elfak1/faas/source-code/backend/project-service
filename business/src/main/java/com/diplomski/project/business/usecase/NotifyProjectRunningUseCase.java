package com.diplomski.project.business.usecase;

import com.diplomski.project.business.boundary.gateway.NotificationService;
import com.diplomski.project.business.boundary.repository.DeploymentRepository;
import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.NotifyProjectRunningUseCaseRequest;
import com.diplomski.project.business.model.value.DeploymentStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotifyProjectRunningUseCase implements UseCase<NotifyProjectRunningUseCaseRequest, Void> {

    private final DeploymentRepository deploymentRepository;
    private final NotificationService notificationService;

    @Override
    public Void execute(NotifyProjectRunningUseCaseRequest useCaseRequest) {
        deploymentRepository.setDeploymentStatusByProjectId(useCaseRequest.getProjectId(), DeploymentStatus.RUNNING);

        notificationService.sendDeploymentRunningMessage(useCaseRequest.getProjectId());

        return null;
    }
}
