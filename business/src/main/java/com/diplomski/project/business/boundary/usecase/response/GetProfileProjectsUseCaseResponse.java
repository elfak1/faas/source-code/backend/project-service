package com.diplomski.project.business.boundary.usecase.response;

import com.diplomski.project.business.model.Project;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;

@Getter
@RequiredArgsConstructor
public class GetProfileProjectsUseCaseResponse {

    private final Page<Project> projectPage;
}
