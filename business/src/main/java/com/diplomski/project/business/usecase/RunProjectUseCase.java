package com.diplomski.project.business.usecase;

import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.RunProjectUseCaseRequest;
import com.diplomski.project.business.boundary.usecase.response.RunProjectUseCaseResponse;
import com.diplomski.project.business.exception.DeploymentAlreadyRunningException;
import com.diplomski.project.business.exception.UnauthorizedToRunProjectException;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.services.deployment.RunDeploymentService;
import com.diplomski.project.business.services.project.GetProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RunProjectUseCase implements UseCase<RunProjectUseCaseRequest, RunProjectUseCaseResponse> {

    private final RunDeploymentService runDeploymentService;
    private final GetProjectService getProjectService;

    @Override
    public RunProjectUseCaseResponse execute(RunProjectUseCaseRequest useCaseRequest) {
        final Project project = getProjectService.getProject(useCaseRequest.getProjectId());
        final Deployment deployment = project.getDeployment();

        if (!project.getProjectOwnerProfileId().equals(useCaseRequest.getProfileId())) {
            throw new UnauthorizedToRunProjectException();
        }

        if (!deployment.getDeploymentStatus().isDeployableStatus()) {
            throw new DeploymentAlreadyRunningException(project);
        }

        runDeploymentService.runDeployment(deployment);

        return RunProjectUseCaseResponse.builder()
                .project(project)
                .build();
    }


}
