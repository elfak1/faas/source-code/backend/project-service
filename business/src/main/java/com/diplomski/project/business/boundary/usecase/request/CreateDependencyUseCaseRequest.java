package com.diplomski.project.business.boundary.usecase.request;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CreateDependencyUseCaseRequest {

    private final String name;

    private final String value;
}
