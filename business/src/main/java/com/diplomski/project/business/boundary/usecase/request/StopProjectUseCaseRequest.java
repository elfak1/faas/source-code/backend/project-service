package com.diplomski.project.business.boundary.usecase.request;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class StopProjectUseCaseRequest {

    private final String projectId;

    private final String profileId;
}
