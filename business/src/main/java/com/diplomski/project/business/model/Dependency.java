package com.diplomski.project.business.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Dependency {

    private final String id;

    private final String name;

    private final String value;

    public Dependency(String name, String value) {
        this.id = null;
        this.name = name;
        this.value = value;
    }
}
