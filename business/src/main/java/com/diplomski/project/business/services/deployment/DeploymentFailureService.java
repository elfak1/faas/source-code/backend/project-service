package com.diplomski.project.business.services.deployment;

import com.diplomski.project.business.boundary.repository.DeploymentRepository;
import com.diplomski.project.business.exception.ItemNotFoundException;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.value.DeploymentStatus;
import com.diplomski.project.business.services.proxy.DelayedExecutionProxy;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeploymentFailureService {

    final DelayedExecutionProxy delayedExecutionProxy;
    final DeploymentRepository deploymentRepository;

    private final static int FIVE_MINUTES_DELAY_IN_MILLISECONDS = 300000;

    public void checkDeploymentFailureAfterTimeout(final String deploymentId) {
        delayedExecutionProxy.executeWithDelay(() -> {
            final Deployment deployment = deploymentRepository.findDeployment(deploymentId)
                    .orElseThrow(() -> new ItemNotFoundException("Deployment not found."));

            if (deployment.getDeploymentStatus().equals(DeploymentStatus.STARTING)) {
                deploymentRepository.setDeploymentStatus(deploymentId, DeploymentStatus.FAILED);
            }

        }, FIVE_MINUTES_DELAY_IN_MILLISECONDS);
    }
}
