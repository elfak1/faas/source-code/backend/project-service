package com.diplomski.project.business.usecase;

import com.diplomski.project.business.boundary.git.GitService;
import com.diplomski.project.business.boundary.repository.ProjectRepository;
import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.CreateProjectUseCaseRequest;
import com.diplomski.project.business.boundary.usecase.response.CreateProjectUseCaseResponse;
import com.diplomski.project.business.exception.CreateDeploymentException;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.model.factory.ProjectFactory;
import com.diplomski.project.business.services.deployment.DeploymentFailureService;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateProjectUseCase implements UseCase<CreateProjectUseCaseRequest, CreateProjectUseCaseResponse> {

    private final ProjectRepository projectRepository;
    private final ProjectFactory projectFactory;
    private final GitService gitService;
    private final DeploymentFailureService deploymentFailureService;

    @Override
    public CreateProjectUseCaseResponse execute(final CreateProjectUseCaseRequest createProjectUseCaseRequest) {
        final Project project = projectFactory.createProject(createProjectUseCaseRequest);

        final Project persistedProject = projectRepository.saveProject(project);

        final Deployment deployment = Try.of(() -> gitService.createAndRunDeployment(persistedProject))
                .onFailure(throwable -> projectRepository.deleteProjectById(persistedProject.getId()))
                .getOrElseThrow(CreateDeploymentException::new);

        //TODO : move this to a event based system with a durable queue to ensure this operation will be successful
        setProjectDeployment(persistedProject, deployment);

        return new CreateProjectUseCaseResponse(persistedProject);
    }

    @Async
    private void setProjectDeployment(final Project project, final Deployment deployment) {
        final Project updatedProject = projectRepository.setProjectDeployment(project.getId(), deployment);
        final String deploymentId = updatedProject.getDeployment().getId();

        deploymentFailureService.checkDeploymentFailureAfterTimeout(deploymentId);
    }
}