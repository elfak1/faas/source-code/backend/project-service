package com.diplomski.project.business.exception;

public class ProjectAlreadyExistsException extends RuntimeException {

    public ProjectAlreadyExistsException(String projectName, String profileId) {
        super(String.format(
                "The profile with id: '%s' already has a project with the name '%s'.",
                profileId,
                projectName));
    }
}
