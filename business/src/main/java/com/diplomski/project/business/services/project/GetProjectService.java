package com.diplomski.project.business.services.project;

import com.diplomski.project.business.boundary.repository.ProjectRepository;
import com.diplomski.project.business.boundary.repository.request.FindProjectRepoRequest;
import com.diplomski.project.business.boundary.repository.response.FindProjectRepoResponse;
import com.diplomski.project.business.exception.ItemNotFoundException;
import com.diplomski.project.business.model.Project;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetProjectService {

    private final ProjectRepository projectRepository;

    public Project getProject(final String projectId) {
        final FindProjectRepoRequest getProjectRepoResponse = FindProjectRepoRequest.builder()
                .projectId(projectId)
                .build();

        final FindProjectRepoResponse projectRepoResponse = projectRepository
                .findProject(getProjectRepoResponse);

        return projectRepoResponse.getProjectOptional()
                .orElseThrow(() -> new ItemNotFoundException("Project not found."));
    }
}
