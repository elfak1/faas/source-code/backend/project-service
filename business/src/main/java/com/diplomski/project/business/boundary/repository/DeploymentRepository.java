package com.diplomski.project.business.boundary.repository;

import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.value.DeploymentStatus;

import java.util.Optional;

public interface DeploymentRepository {

    Optional<Deployment> findDeployment(final String deploymentId);

    void setDeploymentStatus(final String deploymentId, final DeploymentStatus deploymentStatus);

    void setDeploymentScaleAndStatus(final String deploymentId, final Integer deploymentScale, final DeploymentStatus deploymentStatus);

    void setDeploymentStatusByProjectId(final String projectId, final DeploymentStatus deploymentStatus);
}
