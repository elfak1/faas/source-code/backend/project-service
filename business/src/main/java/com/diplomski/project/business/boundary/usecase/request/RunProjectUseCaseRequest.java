package com.diplomski.project.business.boundary.usecase.request;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RunProjectUseCaseRequest {

    private final String projectId;

    private final String profileId;
}
