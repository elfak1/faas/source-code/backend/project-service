package com.diplomski.project.business.usecase;

import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.DeleteProjectUseCaseRequest;
import com.diplomski.project.business.exception.UnauthorizedToDeleteProjectException;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.services.project.DeleteProjectService;
import com.diplomski.project.business.services.project.GetProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteProjectUseCase implements UseCase<DeleteProjectUseCaseRequest, Void> {

    private final GetProjectService getProjectService;
    private final DeleteProjectService deleteProjectService;

    @Override
    public Void execute(DeleteProjectUseCaseRequest useCaseRequest) {
        final Project project = getProjectService.getProject(useCaseRequest.getProjectId());

        if (!project.validateProjectOwner(useCaseRequest.getProfileId())) {
            throw new UnauthorizedToDeleteProjectException();
        }

        deleteProjectService.deleteProject(project);

        return null;
    }
}
