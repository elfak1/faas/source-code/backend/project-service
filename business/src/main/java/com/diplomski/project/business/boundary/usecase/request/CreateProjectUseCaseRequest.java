package com.diplomski.project.business.boundary.usecase.request;

import com.diplomski.project.business.model.value.ProgrammingLanguage;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class CreateProjectUseCaseRequest {

    private final ProgrammingLanguage language;

    private final String profileId;

    private final String description;

    private final String userCode;

    private final String projectName;

    private final List<CreateDependencyUseCaseRequest> createDependencyUseCaseRequests;
}