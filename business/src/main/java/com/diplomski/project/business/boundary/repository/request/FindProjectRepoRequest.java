package com.diplomski.project.business.boundary.repository.request;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class FindProjectRepoRequest {
    private final String projectId;
}
