package com.diplomski.project.business.model.value;

import java.util.List;

public enum DeploymentStatus {
    RUNNING,
    STARTING,
    STOPPED,
    FAILED;

    public boolean isDeployableStatus() {
        return getDeployableStatuses().contains(this);
    }

    public boolean isStoppableStatus() {
        return getStoppableStatuses().contains(this);
    }

    private List<DeploymentStatus> getDeployableStatuses() {
        return List.of(STOPPED, FAILED);
    }

    private List<DeploymentStatus> getStoppableStatuses() {
        return List.of(RUNNING);
    }
}
