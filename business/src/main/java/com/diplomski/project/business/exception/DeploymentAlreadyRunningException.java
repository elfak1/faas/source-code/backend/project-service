package com.diplomski.project.business.exception;

import com.diplomski.project.business.model.Project;

public class DeploymentAlreadyRunningException extends RuntimeException {

    public DeploymentAlreadyRunningException(final Project deployment) {
        super(String.format("project with id '%s' is already running", deployment.getId()));
    }
}
