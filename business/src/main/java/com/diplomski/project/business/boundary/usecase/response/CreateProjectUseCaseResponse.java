package com.diplomski.project.business.boundary.usecase.response;

import com.diplomski.project.business.model.Project;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class CreateProjectUseCaseResponse {
    private final Project project;
}
