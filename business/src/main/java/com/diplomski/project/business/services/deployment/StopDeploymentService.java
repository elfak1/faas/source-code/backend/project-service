package com.diplomski.project.business.services.deployment;

import com.diplomski.project.business.boundary.git.GitService;
import com.diplomski.project.business.boundary.repository.DeploymentRepository;
import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.value.DeploymentStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class StopDeploymentService {

    private final GitService gitService;
    final DeploymentRepository deploymentRepository;

    @Transactional
    public void stopDeployment(final Deployment deployment) {

        deploymentRepository.setDeploymentStatus(deployment.getId(), DeploymentStatus.STOPPED);

        gitService.stopDeployment(deployment);
    }
}
