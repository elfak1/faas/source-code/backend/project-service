package com.diplomski.project.business.boundary.usecase.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class DeleteProjectUseCaseRequest {

    private final String projectId;

    private final String profileId;
}
