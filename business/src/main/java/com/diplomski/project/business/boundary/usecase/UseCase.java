package com.diplomski.project.business.boundary.usecase;

public interface UseCase<T, R> {

    R execute(final T useCaseRequest);
}
