package com.diplomski.project.business.services.project;

import com.diplomski.project.business.boundary.git.GitService;
import com.diplomski.project.business.boundary.repository.ProjectRepository;
import com.diplomski.project.business.model.Project;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class DeleteProjectService {

    private final ProjectRepository projectRepository;
    private final GitService gitService;

    @Transactional
    public void deleteProject(final Project project) {

        projectRepository.deleteProjectById(project.getId());

        gitService.deleteDeployment(project.getDeployment());
    }
}
