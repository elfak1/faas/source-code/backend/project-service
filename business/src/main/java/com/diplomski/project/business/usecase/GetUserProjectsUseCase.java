package com.diplomski.project.business.usecase;

import com.diplomski.project.business.boundary.repository.ProjectRepository;
import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.GetProfileProjectsUseCaseRequest;
import com.diplomski.project.business.boundary.usecase.response.GetProfileProjectsUseCaseResponse;
import com.diplomski.project.business.model.Project;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetUserProjectsUseCase implements UseCase<GetProfileProjectsUseCaseRequest, GetProfileProjectsUseCaseResponse> {

    private final ProjectRepository projectRepository;


    @Override
    public GetProfileProjectsUseCaseResponse execute(GetProfileProjectsUseCaseRequest useCaseRequest) {

        final Page<Project> projectPage = projectRepository
                .findByProjectOwner(useCaseRequest.getProfileId(), useCaseRequest.getPageable());

        return new GetProfileProjectsUseCaseResponse(projectPage);
    }
}
