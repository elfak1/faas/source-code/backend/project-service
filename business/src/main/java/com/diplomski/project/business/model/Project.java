package com.diplomski.project.business.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Project {

    private final String id;

    private final String name;

    private final String description;

    private final String projectOwnerProfileId;

    private final UserCode userCode;

    private final Deployment deployment;

    public Project(String name, String description, String projectOwnerProfileId, UserCode userCode) {
        this.id = null;
        this.name = name;
        this.description = description;
        this.projectOwnerProfileId = projectOwnerProfileId;
        this.userCode = userCode;
        this.deployment = null;
    }

    public String getExecuteEndpoint() {
        return String.format("http://elfakfaas.xyz/faas/%s/execute", this.id);
    }

    public String getCodeInfoEndpoint() {
        return String.format("http://elfakfaas.xyz/faas/%s/code", this.id);
    }

    public boolean validateProjectOwner(final String profileId) {
        return projectOwnerProfileId.equals(profileId);
    }
}