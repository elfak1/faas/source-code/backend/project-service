package com.diplomski.project.business.model;

import com.diplomski.project.business.model.value.ProgrammingLanguage;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class UserCode {

    private final String id;

    private final String code;

    private final ProgrammingLanguage programmingLanguage;

    private final List<Dependency> dependencies;

    public UserCode(String code, ProgrammingLanguage programmingLanguage, List<Dependency> dependencies) {
        this.id = null;
        this.code = code;
        this.programmingLanguage = programmingLanguage;
        this.dependencies = dependencies;
    }
}