package com.diplomski.project.business.model.factory;

import com.diplomski.project.business.boundary.repository.ProjectRepository;
import com.diplomski.project.business.boundary.usecase.request.CreateProjectUseCaseRequest;
import com.diplomski.project.business.exception.ProjectAlreadyExistsException;
import com.diplomski.project.business.model.Dependency;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.model.UserCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ProjectFactory {

    private final ProjectRepository projectRepository;


    public Project createProject(final CreateProjectUseCaseRequest createProjectUseCaseRequest) {
        final String profileId = createProjectUseCaseRequest.getProfileId();
        final String projectName = createProjectUseCaseRequest.getProjectName();
        final String description = createProjectUseCaseRequest.getDescription();

        if (projectRepository.existsByProjectNameAndProfileId(projectName, profileId)) {
            throw new ProjectAlreadyExistsException(projectName, profileId);
        }

        final List<Dependency> dependencies = createProjectUseCaseRequest.getCreateDependencyUseCaseRequests().stream()
                .map(createDependencyRequest ->
                        new Dependency(createDependencyRequest.getName(), createDependencyRequest.getValue()))
                .collect(Collectors.toList());

        final UserCode userCode = new UserCode(
                createProjectUseCaseRequest.getUserCode(),
                createProjectUseCaseRequest.getLanguage(),
                dependencies
        );

        return new Project(
                projectName,
                description,
                profileId,
                userCode);
    }
}