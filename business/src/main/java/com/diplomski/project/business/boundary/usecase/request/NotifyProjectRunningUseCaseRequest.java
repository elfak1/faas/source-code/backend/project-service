package com.diplomski.project.business.boundary.usecase.request;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class NotifyProjectRunningUseCaseRequest {

    private final String projectId;
}
