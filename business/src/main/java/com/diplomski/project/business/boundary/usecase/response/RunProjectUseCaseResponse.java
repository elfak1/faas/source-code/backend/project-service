package com.diplomski.project.business.boundary.usecase.response;

import com.diplomski.project.business.model.Project;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RunProjectUseCaseResponse {

    private final Project project;
}
