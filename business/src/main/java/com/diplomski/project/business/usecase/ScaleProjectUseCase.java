package com.diplomski.project.business.usecase;

import com.diplomski.project.business.boundary.usecase.UseCase;
import com.diplomski.project.business.boundary.usecase.request.ScaleProjectUseCaseRequest;
import com.diplomski.project.business.exception.UnauthorizedToDeleteProjectException;
import com.diplomski.project.business.model.Project;
import com.diplomski.project.business.services.deployment.ScaleDeploymentService;
import com.diplomski.project.business.services.project.GetProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ScaleProjectUseCase implements UseCase<ScaleProjectUseCaseRequest, Void> {

    private final GetProjectService getProjectService;
    private final ScaleDeploymentService scaleDeploymentService;

    @Override
    public Void execute(ScaleProjectUseCaseRequest useCaseRequest) {

        final Project project = getProjectService.getProject(useCaseRequest.getProjectId());

        if (!project.validateProjectOwner(useCaseRequest.getProfileId())) {
            throw new UnauthorizedToDeleteProjectException();
        }

        scaleDeploymentService.scaleDeployment(project.getDeployment(), useCaseRequest.getScaleFactor());

        return null;
    }
}
