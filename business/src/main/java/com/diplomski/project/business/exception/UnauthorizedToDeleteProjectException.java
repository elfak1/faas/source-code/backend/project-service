package com.diplomski.project.business.exception;

public class UnauthorizedToDeleteProjectException extends RuntimeException {


    public UnauthorizedToDeleteProjectException() {
        super("You are not authorized to delete this project.");
    }
}
