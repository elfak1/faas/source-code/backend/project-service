package com.diplomski.project.business.services.proxy;

import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class DelayedExecutionProxy {

    final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(10);

    public void executeWithDelay(final Runnable runnable, final int delayInMilliseconds) {
        executor.schedule(runnable, delayInMilliseconds, TimeUnit.MILLISECONDS);
    }
}
