package com.diplomski.project.business.boundary.usecase.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;

@Getter
@RequiredArgsConstructor
public class GetProfileProjectsUseCaseRequest {

    private final String profileId;

    private final Pageable pageable;
}
