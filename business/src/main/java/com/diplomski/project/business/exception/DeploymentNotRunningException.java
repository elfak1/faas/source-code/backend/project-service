package com.diplomski.project.business.exception;

import com.diplomski.project.business.model.Project;

public class DeploymentNotRunningException extends RuntimeException {

    public DeploymentNotRunningException(final Project deployment) {
        super(String.format("project with id '%s' is not running", deployment.getId()));
    }
}
