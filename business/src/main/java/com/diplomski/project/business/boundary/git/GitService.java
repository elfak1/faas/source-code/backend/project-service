package com.diplomski.project.business.boundary.git;

import com.diplomski.project.business.model.Deployment;
import com.diplomski.project.business.model.Project;

public interface GitService {

    Deployment createAndRunDeployment(final Project project);

    void deleteDeployment(final Deployment deployment);

    void runDeployment(final Deployment deployment);

    void stopDeployment(final Deployment deployment);

    void scaleDeployment(final Deployment deployment, final Integer scaleFactor);
}