package com.diplomski.project.business.boundary.gateway;

public interface NotificationService {

    void sendDeploymentRunningMessage(final String projectId);
}
