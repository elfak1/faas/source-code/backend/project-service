package com.diplomski.project.business.exception;

public class UnauthorizedToRunProjectException extends RuntimeException {

    public UnauthorizedToRunProjectException() {
        super("You are not authorized to run this project.");
    }
}
