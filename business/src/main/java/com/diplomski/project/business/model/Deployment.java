package com.diplomski.project.business.model;

import com.diplomski.project.business.model.value.DeploymentStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Deployment {

    private final String id;

    private final String repositoryUrl;

    private final String externalId;

    private final String pipelineTriggerToken;

    private final DeploymentStatus deploymentStatus;

    private final Integer scaleFactor;

    public Deployment(
            final String repositoryUrl,
            final String externalId,
            final String pipelineTriggerToken,
            final DeploymentStatus deploymentStatus,
            final Integer scaleFactor) {
        this.id = null;
        this.repositoryUrl = repositoryUrl;
        this.externalId = externalId;
        this.pipelineTriggerToken = pipelineTriggerToken;
        this.deploymentStatus = deploymentStatus;
        this.scaleFactor = scaleFactor;
    }
}
